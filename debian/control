Source: paping
Section: net
Priority: optional
Maintainer: Rony Joao de Sousa <ronyjdesousa@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Homepage: https://github.com/ronyjdesousa/paping
Vcs-Browser: https://salsa.debian.org/debian/paping
Vcs-Git: https://salsa.debian.org/debian/paping.git

Package: paping
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: test network connection between two hosts
 Paping is a tool designed to monitor the availability of a TCP/IP network
 connection between two points using TCP protocol.
 .
 It offers a straightforward approach to checking if a server is
 reachable and responsive on a specific port. Paping enables real-time
 monitoring of server status, aiding in the prompt detection of
 connectivity issues.
 .
 Paping is similar to Ping, but at the TCP level, targeting specific ports TCP.
 Both network diagnostic tools are used to test the reachability of a host.
 Here are their key similarities:
  - Purpose: Both tools are used to check the availability and responsiveness
    of network connections.
  - Functionality: They send packets to a specified address and wait for
    a response, measuring the round-trip time.
  - Output: Both provide feedback on the success or failure of the packets sent,
    including response times.
  - Usage: Both can be used to diagnose network issues and ensure that a host
    is reachable over the network.
 .
 Paping is also an excellent tool for learning TCP/IP. However, it do not
 support UDP and IPv6.
